from config.config import RIDE_STATUS_DATA_ADDRESS, RIDE_DATA_ADDRESS, LOCATION_DATA_ADDRESS, TAGGED_RIDE_ADDRESS, \
    COOR_ADDRESS, TESTS_ADDRESS, TIME_FACTORS, RESULT_ADDRESS
from util.spark import spark
from pyspark.sql.types import *
import csv

TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss"

ride_status_schema = StructType([
    StructField("id", IntegerType(), False),
    StructField("rideId", IntegerType(), True),
    StructField("status", StringType(), True),
    StructField("createdAt", TimestampType(), True),
    StructField("updatedAt", TimestampType(), True)])

ride_schema = StructType([
    StructField("id", IntegerType(), True),
    StructField("driverId", IntegerType(), True),
    StructField("createdAt", TimestampType(), True)])

loc_schema = StructType([
    StructField("id", StringType(), False),
    StructField("driverId", IntegerType(), False),
    StructField("upd", StringType(), True),
    StructField("longitude", DoubleType(), True),
    StructField("latitude", DoubleType(), True),
    StructField("accuracy", FloatType(), True),
    StructField("source", StringType(), True),
    StructField("time", LongType(), True),
    StructField("reliable", StringType(), True),
    StructField("cached", StringType(), True),
    StructField("createdAt", TimestampType(), False),
    StructField("speed", StringType(), True),
    StructField("bearing", StringType(), True)])

coor_schema = StructType([
    StructField("id", IntegerType(), True),
    StructField("longitude", DoubleType(), True),
    StructField("latitude", DoubleType(), True),
    StructField("address", StringType(), True),
    StructField("neighbourhoodCode", StringType(), True),
    StructField("congestionControl", StringType(), True),
    StructField("type", StringType(), True),
    StructField("createdAt", TimestampType(), True),
    StructField("updatedAt", TimestampType(), True),
    StructField("rideId", IntegerType(), True),
    StructField("seqNum", StringType(), True),
    StructField("shortAddress", StringType(), True)])

tagged_ride_schema = StructType([
    StructField("id", IntegerType(), True),
    StructField("createdAt", TimestampType(), True),
    StructField("onBoardTime", TimestampType(), True),
    StructField("finishTime", TimestampType(), True),
    StructField("pickup_latitude", FloatType(), True),
    StructField("pickup_longitude", FloatType(), True),
    StructField("pickup_time", TimestampType(), True),
    StructField("drop_latitude", FloatType(), True),
    StructField("drop_longitude", FloatType(), True),
    StructField("drop_time", TimestampType(), True),
])

test_cases_schema = StructType([
    StructField("rideId", IntegerType(), True),
    StructField("eta", IntegerType(), True),
    StructField("ata", IntegerType(), True),
    StructField("createdAt", TimestampType(), True),
])

time_factors_schema = StructType([
    StructField("window", IntegerType(), True),
    StructField("average", DoubleType(), True),
    StructField("cnt", IntegerType(), True),
    StructField("var", DoubleType(), True)
])


#
# result_schema = StructType([
#     StructField("id", IntegerType(), True),
#     StructField("duration", IntegerType(), True),
#     StructField("createdAt", TimestampType(), True),
#     StructField("eta", DoubleType(), True)
# ])


def load_ride_status():
    return spark.read.schema(ride_status_schema) \
        .option("header", False) \
        .option("timestampFormat", TIMESTAMP_FORMAT) \
        .csv(RIDE_STATUS_DATA_ADDRESS)


def load_ride():
    return spark.read.schema(ride_schema) \
        .option("header", False) \
        .option("timestampFormat", TIMESTAMP_FORMAT) \
        .csv(RIDE_DATA_ADDRESS)


def load_location():
    location = spark.read.csv(
        LOCATION_DATA_ADDRESS,
        schema=loc_schema,
        timestampFormat=TIMESTAMP_FORMAT,
        header=False)
    return location


def save_tagged_ride(tagged_ride):
    tagged_ride.write.mode('overwrite').csv(TAGGED_RIDE_ADDRESS)


def load_coor():
    ride = spark.read.csv(
        COOR_ADDRESS,
        schema=coor_schema,
        timestampFormat=TIMESTAMP_FORMAT,
        header=False)
    return ride


def load_full_ride():
    ride = spark.read.csv(
        TAGGED_RIDE_ADDRESS,
        schema=tagged_ride_schema,
        timestampFormat=TIMESTAMP_FORMAT,
        header=False)
    return ride


def load_tests():
    return spark.read.csv(
        TESTS_ADDRESS,
        schema=test_cases_schema,
        timestampFormat=TIMESTAMP_FORMAT,
        header=False
    )


def load_time_factors():
    return spark.read.csv(
        TIME_FACTORS,
        schema=time_factors_schema,
        timestampFormat=TIMESTAMP_FORMAT,
        header=True
    )


def save_result(rows):
    with open(RESULT_ADDRESS, 'a') as f:
        writer = csv.writer(f)
        for row in rows:
            writer.writerow(row)
