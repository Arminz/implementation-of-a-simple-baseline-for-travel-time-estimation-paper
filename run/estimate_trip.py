import numpy as np
from haversine import haversine
from pyspark.sql.functions import col, udf, lit
from scipy.spatial import cKDTree
from datetime import datetime

from config.config import SIMILARITY_DISTANCE, MIN_ACCEPTABLE_DURATION, TRAIN_RIDES_END_TIME
from dataaccess.spark import *


def get_distance(origin_latitude, origin_longitude, destination_latitude, destination_longitude):
    return haversine((origin_latitude, origin_longitude),
                     (destination_latitude, destination_longitude))


udf_distance = udf(get_distance, DoubleType())

schema = StructType([
    StructField("id", IntegerType(), True),
    StructField("createdAt", TimestampType(), True),
    StructField("onBoardTime", TimestampType(), True),
    StructField("finishTime", TimestampType(), True),
    StructField("pickup_latitude", FloatType(), True),
    StructField("pickup_longitude", FloatType(), True),
    StructField("pickup_time", TimestampType(), True),
    StructField("drop_latitude", FloatType(), True),
    StructField("drop_longitude", FloatType(), True),
    StructField("drop_time", TimestampType(), True),
    StructField("duration", IntegerType(), True)
])


def find_similar_trips(origin, destination, train_ride_arr, source_point_tree, destination_point_tree):
    source_filtered_indices = source_point_tree \
        .query_ball_point([origin[0], origin[1]], SIMILARITY_DISTANCE)
    destination_filtered_indices = destination_point_tree \
        .query_ball_point([destination[0], destination[1]], SIMILARITY_DISTANCE)
    distance_filtered = list(set(source_filtered_indices).intersection(destination_filtered_indices))
    print('count of similar trips: {}'.format(len(distance_filtered)))

    filtered_ride_arr = train_ride_arr[distance_filtered]
    space_similar_df = spark.createDataFrame(filtered_ride_arr.tolist(), schema)

    return space_similar_df


def duration(startTime, finishTime):
    delta = finishTime - startTime
    return delta.seconds


udf_duration = udf(duration, IntegerType())


def get_window(time):
    return int((time.minute + (time.hour * 60) + (time.weekday() * 60 * 24)) / 60)


def get_timeFactor(window, time_factors_dict):
    return time_factors_dict[window]


udf_window = udf(get_window, IntegerType())


def udf_timeFactor(time_factors_dict):
    return udf(lambda l: get_timeFactor(l, time_factors_dict), DoubleType())


def get_tagged_similar_trips(origin, destination, time, train_ride_arr,
                             source_point_tree, destination_point_tree, time_factors_dict):
    window = get_window(time)

    current_time_factor = get_timeFactor(window, time_factors_dict)

    similar_trips = find_similar_trips(origin, destination, train_ride_arr, source_point_tree, destination_point_tree)

    average_duration = similar_trips \
        .withColumn('duration', udf_duration('onBoardTime', 'finishTime')) \
        .withColumn('window', udf_window('createdAt')) \
        .withColumn('time_factor', udf_timeFactor(time_factors_dict)(col('window'))) \
        .withColumn('estimated_time', col('duration') * col('time_factor') / current_time_factor)

    return average_duration


def estimate_time(origin, destination, time, train_ride_arr,
                  source_point_tree, destination_point_tree, time_factors_dict):
    tagged = get_tagged_similar_trips(origin, destination, time, train_ride_arr, source_point_tree,
                                      destination_point_tree, time_factors_dict)
    return tagged.agg({'estimated_time': 'avg'}).collect()[0][0]


def main():
    start_time = datetime.now()
    tests = load_tests().withColumnRenamed('rideId', 'id')
    tagged_ride = load_full_ride()
    tagged_ride = tagged_ride \
        .withColumn('duration', udf_duration('onBoardTime', 'finishTime')) \
        .filter(col('duration') > MIN_ACCEPTABLE_DURATION)
    train_ride = tagged_ride \
        .filter(col('createdAt') < TRAIN_RIDES_END_TIME)
    train_ride_list = train_ride.collect()
    train_ride_arr = np.array(train_ride_list)

    tagged_ride = tagged_ride.drop('createdAt')
    test_ride = tests.join(tagged_ride, 'id')
    source_point_tree = cKDTree(train_ride_arr[:, 4:6])
    destination_point_tree = cKDTree(train_ride_arr[:, 7:9])

    time_factors = load_time_factors()
    time_factor_pd = time_factors.select('window', 'average').toPandas()
    time_factors_dict = dict(zip(time_factor_pd.window, time_factor_pd.average))

    cnt = 0
    pre_process_done_time = datetime.now()
    print('pre process took {}'.format(pre_process_done_time - start_time))
    results = []
    for test in test_ride \
            .select('id', 'eta', 'ata', 'createdAt', 'pickup_latitude', 'pickup_longitude', 'drop_latitude',
                    'drop_longitude').collect():

        this_start_time = datetime.now()
        results.append((test[0], test[1], test[2], test[3],
                  estimate_time((test[4], test[5]),
                                (test[6], test[7]),
                                test[3], train_ride_arr,
                                source_point_tree, destination_point_tree, time_factors_dict)
                  ))
        this_finish_time = datetime.now()
        if cnt % 1000 == 0:
            save_result(results)
            results = []
            print('results saved')
        print('simulate {} took {}'.format(cnt, this_finish_time - this_start_time))
        cnt += 1
    save_result(results)
    finish_time = datetime.now()
    print('simulate {} took {}'.format(cnt, finish_time - start_time))


main()
