
# coding: utf-8

# In[5]:


from dataaccess.spark import *


# In[6]:


from datetime import datetime, timedelta
from pyspark.sql.functions import col, collect_list, count, min as sql_min, max as sql_max
from config.config import RIDES_START_TIME, RIDES_END_TIME


# In[7]:


rides = load_ride()


# In[8]:


start_time = RIDES_START_TIME
finish_time = RIDES_END_TIME
start_time_date = datetime.strptime(start_time, '%Y-%m-%d').date()
finish_time_date = datetime.strptime(finish_time, '%Y-%m-%d').date() + timedelta(days=1)


# In[9]:


date_ride = rides.filter(col('createdAt_m')
                         .between(start_time_date, finish_time_date))
date_ride.count()


# In[9]:


date_ride.agg({'createdAt': 'min'}).show()
date_ride.agg({'createdAt': 'max'}).show()


# In[11]:


ride_status = load_ride_status()
date_ride_status = ride_status.filter(col('createdAt_m').between(start_time_date, finish_time_date + timedelta(days=1)))


# In[12]:


ride_status.take(2)


# In[13]:


on_board_status = date_ride_status.filter(col('status') == 'ON_BOARD')
finish_status = date_ride_status.filter(col('status') == 'FINISHED')

start_tagged_ride = date_ride     .join(on_board_status, date_ride['id'] == on_board_status['rideId'])    .select(date_ride['id'], date_ride['createdAt'], date_ride['driverId'], on_board_status['createdAt'].alias('onBoard_time'))
tagged_ride = start_tagged_ride     .join(finish_status, start_tagged_ride['id'] == finish_status['rideId'])    .select(start_tagged_ride['id'], start_tagged_ride['createdAt'], date_ride['driverId'], start_tagged_ride['onBoard_time'], finish_status['createdAt'].alias('finish_time'))

#tagged_ride.take(1)


# In[13]:


tagged_ride.take(1)


# In[30]:


tagged_ride.count()


# In[16]:


(date_ride.count(), start_tagged_ride.count(), tagged_ride.count())


# In[24]:


#tagged_ride.groupBy(col('rideId')).agg(collect_list('status'), count('*').alias('cnt')).filter(col('cnt') < 2).show()


# In[22]:


# ride_status.filter(col('rideId') == 756841936).show()


# In[20]:


# location = load_location()
# date_location = location.filter(col('createdAt_m').between(start_time_date, finish_time_date + timedelta(days=1)))


# In[23]:


# full_ride = tagged_ride.join(date_location,
#                              tagged_ride['driverId'] == location['driverId']) \
#     .filter(location['createdAt'] > tagged_ride['onBoard_time']) \
#     .filter(location['createdAt'] < tagged_ride['finish_time']) \
#     .groupBy(tagged_ride['id']) \
#     .agg(sql_min(location['latitude']).alias('origin_lat'),
#          sql_min(location['longitude']).alias('origin_lon'),
#          sql_max(location['latitude']).alias('destination_lat'),
#          sql_max(location['longitude']).alias('destination_lon'),
#          sql_min(tagged_ride['onBoard_time']).alias('onBoard_time'),
#          sql_max(tagged_ride['finish_time']).alias('finish_time'))
# full_ride


# In[14]:


coor = load_coor()
date_coor = coor.filter(col('createdAt_m').between(start_time_date, finish_time_date + timedelta(days=1)))


# In[15]:


pickup_coor = date_coor.filter(date_coor['type'] == 'Mission_PICKUP')

drop_coor = date_coor.filter(date_coor['type'] == 'Mission_DROP')

pickup_ride = tagged_ride    .join(pickup_coor, tagged_ride['id'] == pickup_coor['rideId'])    .select(tagged_ride['id'], tagged_ride['createdAt'], tagged_ride['onBoard_time'], tagged_ride['finish_time'],
            pickup_coor['latitude'].alias('pickup_latitude'), pickup_coor['longitude'].alias('pickup_longitude'),
            pickup_coor['createdAt'].alias('pickup_time'))

full_ride = pickup_ride    .join(drop_coor, pickup_ride['id'] == drop_coor['rideId'])    .select(pickup_ride['id'], pickup_ride['createdAt'], pickup_ride['onBoard_time'], pickup_ride['finish_time'],
            pickup_ride['pickup_latitude'], pickup_ride['pickup_longitude'], pickup_ride['pickup_time'],
            drop_coor['latitude'].alias('drop_latitude'), drop_coor['longitude'].alias('drop_longitude'),
            drop_coor['createdAt'].alias('drop_time'))


# In[16]:


# full_ride.take(1)


# In[18]:


# full_ride.count()


# In[20]:


save_tagged_ride(full_ride)

