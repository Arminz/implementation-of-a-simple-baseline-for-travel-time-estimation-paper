
# coding: utf-8

# In[1]:


from dataaccess.spark import *


# In[3]:


import pyspark.sql.functions as F
from haversine import haversine
from config.config import *
# from config.config import WINDOW_SIZE, WINDOW_SPEEDS


# In[4]:


from matplotlib import pyplot as plt

# get_ipython().magic(u'matplotlib inline')


# In[5]:


ride = load_full_ride()
ride
ride.agg({'createdAt': 'max'}).show()


# In[7]:


def duration(startTime, finishTime):
    delta = finishTime - startTime
    return delta.seconds


def get_distance(origin_latitude, origin_longitude, destination_latitude, destination_longitude):
    # print(haversine((origin_latitude, origin_longitude),(destination_latitude, destination_longitude)))
    # return (origin_latitude - destination_latitude)
    return haversine((origin_latitude, origin_longitude),
                     (destination_latitude, destination_longitude))


def get_window(time):
    return int((time.minute + (time.hour * 60) + (time.weekday() * 60 * 24)) / 60)

udf_distance = F.udf(get_distance, DoubleType())

udf_duration = F.udf(duration, IntegerType())

udf_window = F.udf(get_window, IntegerType())

duration_ride = ride.withColumn('duration',
                                udf_duration('onBoardTime',
                                             'finishTime'))

duration_ride = duration_ride    .filter(F.col('createdAt') < TRAIN_RIDES_END_TIME)    .filter(F.col('duration') > MIN_ACCEPTABLE_DURATION)

distance_duration_ride = duration_ride     .withColumn('distance',
                udf_distance('pickup_latitude', 'pickup_longitude', 'drop_latitude', 'drop_longitude'))

speed_ride = distance_duration_ride    .withColumn('speed', (F.col('distance') / F.col('duration')) * 3600)

window_speed_ride = speed_ride     .withColumn('window',
                udf_window('createdAt'))

# window_speed_ride.take(1)


# In[9]:


window_speed = window_speed_ride     .groupBy('window')     .agg(F.mean('speed').alias('average'), F.count('*').alias('cnt'), F.variance('speed').alias('var'))
# window_speed.take(1)


# In[11]:


window_speed.cache()


# In[17]:


window_speed.toPandas().to_csv('/home/armin/baseline-data/window_speeds/window-speeds.csv', index=False)


# In[14]:


panda = window_speed.toPandas()


# In[15]:


panda.head()


# In[16]:


# panda.sort_values(by='window').plot(x='window', y='average')
# panda.sort_values(by='window').plot(x='window', y='cnt')


# In[ ]:


# plt.stem(1,1)
# slots = window_speed.rdd.map(lambda row: row.window)
# speeds = window_speed.rdd.map(lambda row: row.average)
# plt.stem(slots[0], speeds[1])

