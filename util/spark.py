from pyspark.sql import SparkSession
from config import config

spark = SparkSession \
    .builder \
    .master("local[{}]".format(config.SPARK_THREADS)) \
    .config("spark.driver.memory", "{}g".format(config.SPARK_DRIVER_MEMORY)) \
    .getOrCreate()

sc = spark.sparkContext
